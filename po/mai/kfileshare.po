# translation of kfileshare.po to Maithili
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Sangeeta Kumari <sangeeta09@gmail.com>, 2009.
msgid ""
msgstr ""
"Project-Id-Version: kfileshare\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-12-15 01:36+0000\n"
"PO-Revision-Date: 2009-04-22 00:07+0530\n"
"Last-Translator: Sangeeta Kumari <sangeeta09@gmail.com>\n"
"Language-Team: Maithili <maithili.sf.net>\n"
"Language: mai\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms: nplurals=2; plural=(n!=1);\n"
"\n"
"\n"

#: samba/aclproperties/plugin.cpp:59
#, kde-format
msgctxt "@option:radio an entry denying permissions"
msgid "Deny"
msgstr ""

#: samba/aclproperties/plugin.cpp:61
#, fuzzy, kde-format
#| msgid "Allow"
msgctxt "@option:radio an entry allowing permissions"
msgid "Allow"
msgstr "स्वीकारू"

#: samba/aclproperties/plugin.cpp:65
#, kde-format
msgctxt ""
"@option:radio an unknown permission entry type (doesn't really happen)"
msgid "Unknown"
msgstr ""

#: samba/aclproperties/plugin.cpp:72
#, kde-format
msgctxt "@option:radio permission applicability type"
msgid "This folder only"
msgstr ""

#: samba/aclproperties/plugin.cpp:74
#, kde-format
msgctxt "@option:radio permission applicability type"
msgid "This folder, subfolders and files"
msgstr ""

#: samba/aclproperties/plugin.cpp:76
#, kde-format
msgctxt "@option:radio permission applicability type"
msgid "This folder and subfolders"
msgstr ""

#: samba/aclproperties/plugin.cpp:78
#, kde-format
msgctxt "@option:radio permission applicability type"
msgid "This folder and files"
msgstr ""

#: samba/aclproperties/plugin.cpp:80
#, kde-format
msgctxt "@option:radio permission applicability type"
msgid "Subfolders and files only"
msgstr ""

#: samba/aclproperties/plugin.cpp:82
#, kde-format
msgctxt "@option:radio permission applicability type"
msgid "Subfolders only"
msgstr ""

#: samba/aclproperties/plugin.cpp:84
#, kde-format
msgctxt "@option:radio permission applicability type"
msgid "Files only"
msgstr ""

#: samba/aclproperties/plugin.cpp:87
#, kde-format
msgctxt "@option:radio permission applicability type (doesn't really happen)"
msgid "Unknown"
msgstr ""

#: samba/aclproperties/plugin.cpp:185
#, fuzzy, kde-format
#| msgid "Permissions"
msgctxt "@title:tab"
msgid "Remote Permissions"
msgstr "अनुमतिसभ"

#: samba/aclproperties/qml/ACEPage.qml:29
msgctxt "@info"
msgid ""
"This permission entry was inherited from a parent container and can only be "
"modified on that parent (e.g. a higher level directory)."
msgstr ""

#: samba/aclproperties/qml/ACEPage.qml:36
msgctxt "@label"
msgid "Type:"
msgstr ""

#: samba/aclproperties/qml/ACEPage.qml:46
msgctxt "@label"
msgid "Applies to:"
msgstr ""

#: samba/aclproperties/qml/ACEPage.qml:59
msgctxt "@option:check"
msgid "Traverse folder / execute file"
msgstr ""

#: samba/aclproperties/qml/ACEPage.qml:63
msgctxt "@option:check"
msgid "List folder / read data"
msgstr ""

#: samba/aclproperties/qml/ACEPage.qml:67
msgctxt "@option:check"
msgid "Read attributes"
msgstr ""

#: samba/aclproperties/qml/ACEPage.qml:71
msgctxt "@option:check"
msgid "Read extended attributes"
msgstr ""

#: samba/aclproperties/qml/ACEPage.qml:75
msgctxt "@option:check"
msgid "Create files / write data"
msgstr ""

#: samba/aclproperties/qml/ACEPage.qml:79
msgctxt "@option:check"
msgid "Create folders / append data"
msgstr ""

#: samba/aclproperties/qml/ACEPage.qml:83
msgctxt "@option:check"
msgid "Write attributes"
msgstr ""

#: samba/aclproperties/qml/ACEPage.qml:87
msgctxt "@option:check"
msgid "Write extended attributes"
msgstr ""

#: samba/aclproperties/qml/ACEPage.qml:91
msgctxt "@option:check"
msgid "Delete subfolders and files"
msgstr ""

#: samba/aclproperties/qml/ACEPage.qml:95
msgctxt "@option:check"
msgid "Delete"
msgstr ""

#: samba/aclproperties/qml/ACEPage.qml:99
#, fuzzy
#| msgid "Permissions"
msgctxt "@option:check"
msgid "Read permissions"
msgstr "अनुमतिसभ"

#: samba/aclproperties/qml/ACEPage.qml:103
#, fuzzy
#| msgid "Permissions"
msgctxt "@option:check"
msgid "Change permissions"
msgstr "अनुमतिसभ"

#: samba/aclproperties/qml/ACEPage.qml:107
msgctxt "@option:check"
msgid "Take ownership"
msgstr ""

#: samba/aclproperties/qml/ACEPage.qml:112
msgctxt "@option:check"
msgid ""
"Only apply these permissions to objects and/or containers within this "
"container"
msgstr ""

#: samba/aclproperties/qml/MainPage.qml:13
msgctxt "@title"
msgid "Access Control Entries"
msgstr ""

#: samba/aclproperties/qml/MainPage.qml:29
#, fuzzy
#| msgid "Owner"
msgctxt "@title file/folder owner info"
msgid "Ownership"
msgstr "मालिक"

#: samba/aclproperties/qml/MainPage.qml:33
#, fuzzy
#| msgid "Owner"
msgctxt "@label"
msgid "Owner:"
msgstr "मालिक"

#: samba/aclproperties/qml/MainPage.qml:37
#, fuzzy
#| msgid "Group"
msgctxt "@label"
msgid "Group:"
msgstr "समूह"

#: samba/aclproperties/qml/NoDataPage.qml:17
#, fuzzy
#| msgid "Permissions"
msgctxt "@info"
msgid "No Permissions Found"
msgstr "अनुमतिसभ"

#: samba/aclproperties/qml/NoDataPage.qml:18
msgctxt "@info"
msgid ""
"There are probably no SMB/Windows/Advanced permissions set on this file."
msgstr ""

#: samba/filepropertiesplugin/authhelper.cpp:47
#, kde-kuit-format
msgctxt "@info"
msgid ""
"User name <resource>%1</resource> is not valid as the name of a Samba user; "
"cannot check for its existence."
msgstr ""

#: samba/filepropertiesplugin/authhelper.cpp:66
#, kde-kuit-format
msgctxt ""
"@info '%1 %2' together make up a terminal command; %3 is the command's output"
msgid "Command <command>%1 %2</command> failed:<nl/><nl/>%3"
msgstr ""

#: samba/filepropertiesplugin/authhelper.cpp:80
#: samba/filepropertiesplugin/authhelper.cpp:126
#, kde-kuit-format
msgctxt "@info error while looking up uid from dbus"
msgid "Could not resolve calling user."
msgstr ""

#: samba/filepropertiesplugin/authhelper.cpp:86
#, kde-format
msgctxt "@info"
msgid ""
"For security reasons, creating Samba users with empty passwords is not "
"allowed."
msgstr ""

#: samba/filepropertiesplugin/authhelper.cpp:131
#, kde-kuit-format
msgctxt "@info"
msgid ""
"<resource>%1</resource> is not a valid group name; cannot make user "
"<resource>%2</resource> a member of it."
msgstr ""

#: samba/filepropertiesplugin/authhelper.cpp:139
#, kde-kuit-format
msgctxt "@info"
msgid ""
"For security reasons, cannot make user <resource>%1</resource> a member of "
"group <resource>%2</"
"resource>.                                                    The group name "
"is insecure; valid group names do "
"not                                                    include the text "
"<resource>admin</resource> or <resource>root</resource>."
msgstr ""

#: samba/filepropertiesplugin/groupmanager.cpp:36
#, kde-kuit-format
msgctxt "@info:status"
msgid "Your Samba installation appears to be broken."
msgstr ""

#: samba/filepropertiesplugin/groupmanager.cpp:40
#, kde-kuit-format
msgctxt "@info:status"
msgid ""
"This error is caused by your distribution not setting up Samba sharing "
"properly. Please file a bug with your distribution or check your "
"distribution's documentation on setting up Samba sharing."
msgstr ""

#: samba/filepropertiesplugin/groupmanager.cpp:42
#, kde-kuit-format
msgctxt "@info:status"
msgid ""
"This error is caused by your distribution not setting up Samba sharing "
"properly. Please file a bug with your distribution or check your "
"distribution's documentation on setting up Samba sharing. Error:<nl/"
"><message>%1</message>"
msgstr ""

#: samba/filepropertiesplugin/groupmanager.cpp:49
#, kde-kuit-format
msgctxt "@info:status"
msgid ""
"This folder can't be shared because <filename>%1</filename> does not exist."
msgstr ""

#: samba/filepropertiesplugin/groupmanager.cpp:51
#, kde-kuit-format
msgctxt "@info:status"
msgid ""
"This error is caused by your distribution not setting up Samba sharing "
"properly. You can fix it yourself by creating that folder manually. Then "
"close and re-open this window."
msgstr ""

#: samba/filepropertiesplugin/groupmanager.cpp:58
#, kde-kuit-format
msgctxt "@info:status"
msgid ""
"This folder can't be shared because your user account isn't a member of the "
"<resource>%1</resource> group."
msgstr ""

#: samba/filepropertiesplugin/groupmanager.cpp:60
#, kde-kuit-format
msgctxt "@info:status"
msgid ""
"You can fix this by making your user a member of that group. Then restart "
"the system."
msgstr ""

#: samba/filepropertiesplugin/groupmanager.cpp:64
#, kde-format
msgctxt "action@button makes user a member of the samba share group"
msgid "Make me a Group Member"
msgstr ""

#: samba/filepropertiesplugin/groupmanager.cpp:73
#, kde-kuit-format
msgctxt "@info:status"
msgid ""
"This folder can't be shared because your user account doesn't have "
"permission to write into <filename>%1</filename>."
msgstr ""

#: samba/filepropertiesplugin/groupmanager.cpp:75
#, kde-kuit-format
msgctxt "@info:status"
msgid ""
"You can fix this by ensuring that the <resource>%1</resource> group has "
"write permission for <filename>%2</filename>. Then close and re-open this "
"window."
msgstr ""

#: samba/filepropertiesplugin/groupmanager.cpp:102
#, kde-kuit-format
msgctxt "@label kauth action description %1 is a username %2 a group name"
msgid ""
"Adding user <resource>%1</resource> to group <resource>%2</resource> so they "
"may configure Samba user shares"
msgstr ""

#: samba/filepropertiesplugin/groupmanager.cpp:113
#, kde-kuit-format
msgctxt "@info"
msgid ""
"Failed to make user <resource>%1</resource> a member of group <resource>%2</"
"resource>"
msgstr ""

#: samba/filepropertiesplugin/permissionshelper.cpp:95
#, fuzzy, kde-format
#| msgid "Path"
msgctxt "@title"
msgid "File Path"
msgstr "पथ"

#: samba/filepropertiesplugin/permissionshelper.cpp:97
#, fuzzy, kde-format
#| msgid "Permissions"
msgctxt "@title"
msgid "Current Permissions"
msgstr "अनुमतिसभ"

#: samba/filepropertiesplugin/permissionshelper.cpp:99
#, fuzzy, kde-format
#| msgid "Permissions"
msgctxt "@title"
msgid "Required Permissions"
msgstr "अनुमतिसभ"

#: samba/filepropertiesplugin/qml/ACLPage.qml:29
msgctxt "@title"
msgid "Denying Access"
msgstr ""

#. i18n markup; this also means newlines are ignored!
#: samba/filepropertiesplugin/qml/ACLPage.qml:43
msgctxt "@info"
msgid ""
"\n"
"Denying access prevents using this share even when another access rule might "
"grant access. A denial rule always\n"
"takes precedence. In particular denying access to <resource>Everyone</"
"resource> actually disables access for everyone.\n"
"It is generally not necessary to deny anyone because the regular directory "
"and file permissions still apply to shared\n"
"directories. A user who does not have access to the directory locally will "
"still not be able to access it remotely even\n"
"when the Share access rules would allow it."
msgstr ""

#: samba/filepropertiesplugin/qml/ACLPage.qml:62
msgctxt "@label"
msgid "This folder needs extra permissions for sharing to work"
msgstr ""

#: samba/filepropertiesplugin/qml/ACLPage.qml:66
#, fuzzy
#| msgid "Permissions"
msgctxt "@action:button opens the change permissions page"
msgid "Fix Permissions"
msgstr "अनुमतिसभ"

#: samba/filepropertiesplugin/qml/ACLPage.qml:78
msgctxt "@label"
msgid ""
"The share might not work properly because share folder or its paths has "
"Advanced Permissions: %1"
msgstr ""

#: samba/filepropertiesplugin/qml/ACLPage.qml:85
msgctxt "@option:check"
msgid "Share this folder with other computers on the local network"
msgstr ""

#: samba/filepropertiesplugin/qml/ACLPage.qml:102
#, fuzzy
#| msgid "Name:"
msgctxt "@label"
msgid "Name:"
msgstr "नाम:"

#: samba/filepropertiesplugin/qml/ACLPage.qml:134
msgctxt "@label"
msgid ""
"This name cannot be used. Share names must not be user names and there must "
"not be two shares with the same name on the entire system."
msgstr ""

#: samba/filepropertiesplugin/qml/ACLPage.qml:143
msgctxt "@label"
msgid ""
"This name may be too long. It can cause interoperability problems or get "
"rejected by Samba."
msgstr ""

#: samba/filepropertiesplugin/qml/ACLPage.qml:150
msgctxt "@option:check"
msgid "Allow guests"
msgstr ""

#: samba/filepropertiesplugin/qml/ACLPage.qml:163
msgctxt "@label"
msgid "Guest access is disabled by the system's Samba configuration."
msgstr ""

#: samba/filepropertiesplugin/qml/ACLPage.qml:253
msgctxt "@option:radio user can read&write"
msgid "Full Control"
msgstr ""

#: samba/filepropertiesplugin/qml/ACLPage.qml:254
#, fuzzy
#| msgid "Read only"
msgctxt "@option:radio user can read"
msgid "Read Only"
msgstr "सिर्फ पढ़बाक लेल"

#: samba/filepropertiesplugin/qml/ACLPage.qml:255
msgctxt "@option:radio user not allowed to access share"
msgid "No Access"
msgstr ""

#: samba/filepropertiesplugin/qml/ACLPage.qml:281
msgctxt "@button"
msgid "Show Samba status monitor"
msgstr ""

#: samba/filepropertiesplugin/qml/ChangePassword.qml:23
#, fuzzy
#| msgid "Password"
msgctxt "@title"
msgid "Set password"
msgstr "कूटशब्द"

#: samba/filepropertiesplugin/qml/ChangePassword.qml:75
#, fuzzy
#| msgid "Password"
msgctxt "@label:textbox"
msgid "Password"
msgstr "कूटशब्द"

#: samba/filepropertiesplugin/qml/ChangePassword.qml:86
msgctxt "@label:textbox"
msgid "Confirm password"
msgstr ""

#: samba/filepropertiesplugin/qml/ChangePassword.qml:97
msgctxt "@label error message"
msgid "Passwords must match"
msgstr ""

#: samba/filepropertiesplugin/qml/ChangePassword.qml:117
#, fuzzy
#| msgid "Password"
msgctxt ""
"@action:button creates a new samba user with the user-specified password"
msgid "Set Password"
msgstr "कूटशब्द"

#: samba/filepropertiesplugin/qml/ChangePermissionsPage.qml:34
msgctxt "@info"
msgid ""
"\n"
"<para>The folder <filename>%1</filename> needs extra permissions for sharing "
"to work.</para>\n"
"<para>Do you want to add these permissions now?</para><nl/>\n"
msgstr ""

#: samba/filepropertiesplugin/qml/ChangePermissionsPage.qml:71
#, fuzzy
#| msgid "Permissions"
msgctxt "@action:button changes permissions"
msgid "Change Permissions"
msgstr "अनुमतिसभ"

#: samba/filepropertiesplugin/qml/ChangePermissionsPage.qml:77
msgctxt "@label"
msgid ""
"Could not change permissions for: %1. All permission changes have been "
"reverted to initial state."
msgstr ""

#: samba/filepropertiesplugin/qml/ChangePermissionsPage.qml:86
#, fuzzy
#| msgid "&Cancel"
msgctxt "@action:button cancels permissions change"
msgid "Cancel"
msgstr "रद्द करू (&C)"

#: samba/filepropertiesplugin/qml/InstallPage.qml:33
#: samba/filepropertiesplugin/qml/MissingSambaPage.qml:17
msgctxt "@info"
msgid ""
"The <application>Samba</application> file sharing service must be installed "
"before folders can be shared."
msgstr ""

#: samba/filepropertiesplugin/qml/InstallPage.qml:36
msgctxt "@button"
msgid "Install Samba"
msgstr ""

#: samba/filepropertiesplugin/qml/InstallPage.qml:44
msgctxt "@label"
msgid "The Samba package failed to install."
msgstr ""

#: samba/filepropertiesplugin/qml/MissingSambaPage.qml:18
msgid ""
"Because this distro does not include PackageKit, we cannot show you a nice "
"\"Install it\" button, and you will have to use your package manager to "
"install the <command>samba</command> server package manually."
msgstr ""

#: samba/filepropertiesplugin/qml/RebootPage.qml:17
msgctxt "@label"
msgid "Restart the computer to complete the changes."
msgstr ""

#: samba/filepropertiesplugin/qml/RebootPage.qml:20
msgctxt "@button restart the system"
msgid "Restart"
msgstr ""

#. i18n markup
#: samba/filepropertiesplugin/qml/UserPage.qml:65
msgctxt "@info"
msgid ""
"\n"
"<para>\n"
"Samba uses a separate user database from the system one.\n"
"This requires you to set a separate Samba password for every user that you "
"want to\n"
"be able to authenticate with.\n"
"</para>\n"
"<para>\n"
"Before you can access shares with your current user account you need to set "
"a Samba password.\n"
"</para>"
msgstr ""

#: samba/filepropertiesplugin/qml/UserPage.qml:80
msgctxt "@action:button opens dialog to create new user"
msgid "Create Samba password"
msgstr ""

#. i18n markup
#: samba/filepropertiesplugin/qml/UserPage.qml:88
msgctxt "@info"
msgid ""
"\n"
"Additional user management and password management can be done using Samba's "
"<command>smbpasswd</command>\n"
"command line utility."
msgstr ""

#: samba/filepropertiesplugin/sambausershareplugin.cpp:155
#, kde-format
msgctxt "@info detailed error messsage"
msgid ""
"You have exhausted the maximum amount of shared directories you may have "
"active at the same time."
msgstr ""

#: samba/filepropertiesplugin/sambausershareplugin.cpp:157
#, fuzzy, kde-format
#| msgid "File sharing is disabled."
msgctxt "@info detailed error messsage"
msgid "The share name is invalid."
msgstr "फाइल साझा अक्षम अछि."

#: samba/filepropertiesplugin/sambausershareplugin.cpp:159
#, kde-format
msgctxt "@info detailed error messsage"
msgid "The share name is already in use for a different directory."
msgstr ""

#: samba/filepropertiesplugin/sambausershareplugin.cpp:161
#, kde-format
msgctxt "@info detailed error messsage"
msgid "The path is invalid."
msgstr ""

#: samba/filepropertiesplugin/sambausershareplugin.cpp:163
#, kde-format
msgctxt "@info detailed error messsage"
msgid "The path does not exist."
msgstr ""

#: samba/filepropertiesplugin/sambausershareplugin.cpp:165
#, kde-format
msgctxt "@info detailed error messsage"
msgid "The path is not a directory."
msgstr ""

#: samba/filepropertiesplugin/sambausershareplugin.cpp:167
#, kde-format
msgctxt "@info detailed error messsage"
msgid "The path is relative."
msgstr ""

#: samba/filepropertiesplugin/sambausershareplugin.cpp:169
#, kde-format
msgctxt "@info detailed error messsage"
msgid "This path may not be shared."
msgstr ""

#: samba/filepropertiesplugin/sambausershareplugin.cpp:171
#, kde-format
msgctxt "@info detailed error messsage"
msgid "The access rule is invalid."
msgstr ""

#: samba/filepropertiesplugin/sambausershareplugin.cpp:173
#, kde-format
msgctxt "@info detailed error messsage"
msgid "An access rule's user is not valid."
msgstr ""

#: samba/filepropertiesplugin/sambausershareplugin.cpp:175
#, kde-format
msgctxt "@info detailed error messsage"
msgid "The 'Guest' access rule is invalid."
msgstr ""

#: samba/filepropertiesplugin/sambausershareplugin.cpp:177
#, kde-format
msgctxt "@info detailed error messsage"
msgid "Enabling guest access is not allowed."
msgstr ""

#: samba/filepropertiesplugin/sambausershareplugin.cpp:195
#, kde-kuit-format
msgctxt "@info error in the underlying binaries. %1 is CLI output"
msgid ""
"<para>An error occurred while trying to share the directory. The share has "
"not been created.</para><para>Samba internals report:</para><message>%1</"
"message>"
msgstr ""

#: samba/filepropertiesplugin/sambausershareplugin.cpp:202
#, kde-format
msgctxt "@info/title"
msgid "Failed to Create Network Share"
msgstr ""

#: samba/filepropertiesplugin/sambausershareplugin.cpp:215
#, kde-kuit-format
msgctxt "@info error in the underlying binaries. %1 is CLI output"
msgid ""
"<para>An error occurred while trying to un-share the directory. The share "
"has not been removed.</para><para>Samba internals report:</para><message>%1</"
"message>"
msgstr ""

#: samba/filepropertiesplugin/sambausershareplugin.cpp:222
#, kde-format
msgctxt "@info/title"
msgid "Failed to Remove Network Share"
msgstr ""

#: samba/filepropertiesplugin/usermanager.cpp:96
#, kde-format
msgctxt "@label kauth action description %1 is a username"
msgid "Checking if Samba user '%1' exists"
msgstr ""

#: samba/filepropertiesplugin/usermanager.cpp:119
#, kde-format
msgctxt "@label kauth action description %1 is a username"
msgid "Creating new Samba user '%1'"
msgstr ""

#, fuzzy
#~| msgid "Share"
#~ msgctxt "@title:tab"
#~ msgid "Share"
#~ msgstr "साझा"

#, fuzzy
#~| msgid "Create"
#~ msgctxt "@title"
#~ msgid "Create User"
#~ msgstr "बनाबू"

#, fuzzy
#~| msgid "Create"
#~ msgctxt "@action:button creates a new samba user"
#~ msgid "Create User"
#~ msgstr "बनाबू"

#~ msgid "&Share"
#~ msgstr "साझेदारी (&S)"

#~ msgid "Name"
#~ msgstr "नाम"

#~ msgid "UID"
#~ msgstr "UID"

#~ msgid "GID"
#~ msgstr "GID"

#~ msgid "&OK"
#~ msgstr "बेस (&O)"

#~ msgid "&Verify:"
#~ msgstr "सत्यापित (&V):"

#~ msgid "&Password:"
#~ msgstr "कूटशब्द: (&P)"

#~ msgid "&Username:"
#~ msgstr "प्रयोक्ता नाम: (&U)"

#~ msgid "&Load"
#~ msgstr "लोड करु (&L)"

#~ msgid "Alt+"
#~ msgstr "Alt+"

#~ msgid "User"
#~ msgstr "प्रयोक्ता"

#~ msgid "Server"
#~ msgstr "सर्वर"

#~ msgid "Help"
#~ msgstr "मद्दति "

#~ msgid "Comment"
#~ msgstr "टिप्पणी"

#~ msgid "Properties"
#~ msgstr "गुण"

#~ msgid "Printer"
#~ msgstr "मुद्रक"

#~ msgid "Disabled"
#~ msgstr "अक्षम"

#~ msgid "Add"
#~ msgstr "जोड़ू"

#~ msgid "Chan&ge Password..."
#~ msgstr "कूटशब्द बदलू... (&g)"

#~ msgid "Security"
#~ msgstr "सुरक्षा"

#~ msgid "&General"
#~ msgstr "सामान्य (&G)"

#~ msgid "General"
#~ msgstr "सामान्य"

#~ msgid "Never"
#~ msgstr "कहियो नहि"

#~ msgid "Client"
#~ msgstr "क्लाएँट"

#~ msgid "Auto"
#~ msgstr "स्वचालित"

#~ msgid "Mandatory"
#~ msgstr "अनिवार्य"

#~ msgid "Yes"
#~ msgstr "हँ"

#~ msgid "No"
#~ msgstr "नहि"

#~ msgid "Logging"
#~ msgstr "लॉग कए रहल अछि"

#~ msgid "Status"
#~ msgstr "स्थिति"

#~ msgid "Modules"
#~ msgstr "मॉड्यूल"

#~ msgid "Switches"
#~ msgstr "स्विच"

#~ msgctxt "minutes"
#~ msgid "Min"
#~ msgstr "न्यूनतम"

#~ msgctxt "mega byte"
#~ msgid "MB"
#~ msgstr "MB"

#~ msgid "Numbers"
#~ msgstr "सँख्यासभ"

#~ msgid "Printing"
#~ msgstr "छपाइ कए रहल अछि"

#~ msgid "Commands"
#~ msgstr "कमांड"

#~ msgid "Domain"
#~ msgstr "डोमेन"

#~ msgid "General Options"
#~ msgstr "सामान्य विकल्प"

#~ msgid "Filenames"
#~ msgstr "फाइलनाम"

#~ msgid "milliseconds"
#~ msgstr "मिलीसेकेंड"

#~ msgid "Charset"
#~ msgstr "चारसेट"

#~ msgid "Character set:"
#~ msgstr "संप्रतीक सेट:"

#~ msgid "Shutdown"
#~ msgstr "बन्न करू"

#~ msgid "SSL"
#~ msgstr "SSL"

#~ msgid "Protocol"
#~ msgstr "प्रोटोकॉल"

#~ msgid "Limits"
#~ msgstr "सीमा"

#~ msgid "Browsing"
#~ msgstr "ब्राउज़िंग "

#~ msgid "LDAP"
#~ msgstr "LDAP"

#~ msgid "Off"
#~ msgstr "बन्न"

#~ msgid "On"
#~ msgstr "शुरू"

#~ msgid "Only"
#~ msgstr "केवल"

#~ msgid "Misc"
#~ msgstr "विविध"

#~ msgid "Miscellaneous"
#~ msgstr "विविध"

#~ msgctxt "minurtes"
#~ msgid "Min"
#~ msgstr "न्यूनतम"

#~ msgid "Debug"
#~ msgstr "डिबग"

#~ msgid "Pixmap"
#~ msgstr "पिक्समैप"

#~ msgid "&Path:"
#~ msgstr "पाथ (&P):"

#~ msgid "Hidden"
#~ msgstr "नुकाएल"

#~ msgid "Size"
#~ msgstr "आकार"

#~ msgid "Date"
#~ msgstr "दिनांक"

#~ msgid "..."
#~ msgstr "..."

#~ msgid "bytes"
#~ msgstr "बाइटसभ"

#~ msgid "Lower"
#~ msgstr "निचला"

#~ msgid "Upper"
#~ msgstr "उप्परी"

#~ msgid "Exec"
#~ msgstr "चलैनाइ"

#~ msgid "&Volume:"
#~ msgstr "आवाज निर्धारक: (&V)"

#~ msgid "&Help"
#~ msgstr "मद्दति (&H)"

#~ msgid "F1"
#~ msgstr "F1"

#~ msgid "Users"
#~ msgstr "प्रयोक्ता"

#~ msgid "Reject"
#~ msgstr "अस्वीकृत करू"

#~ msgid "Access Permissions"
#~ msgstr "पहुँच अनुमतिसभ"

#~ msgid "Others"
#~ msgstr "आन "

#~ msgid "Read"
#~ msgstr "पढ़ू"

#~ msgid "Write"
#~ msgstr "लिखू"

#~ msgid "Sticky"
#~ msgstr "स्टिकी"

#~ msgid "Set GID"
#~ msgstr "जीआईडी सेट करू"

#~ msgid "Set UID"
#~ msgstr "यूआईडी सेट करू"

#~ msgid "Special"
#~ msgstr "विशेष"

#~ msgid "User Settings"
#~ msgstr "प्रयोक्ता निर्धारण"

#~ msgid "Parameters"
#~ msgstr "पैरामीटर"

#~ msgid "Options"
#~ msgstr "विकल्प"

#~ msgid "FF"
#~ msgstr "FF"

#~ msgid "Folder:"
#~ msgstr "फोल्डर:"

#~ msgid "Add User"
#~ msgstr "प्रयोक्ता जोड़ू"

#~ msgid "Shared Folders"
#~ msgstr "साझेदारी फोल्डर"

#~ msgid "Samba"
#~ msgstr "साम्बा"

#~ msgid "NFS"
#~ msgstr "NFS"

#~ msgid "A&dd..."
#~ msgstr "जोड़ू (&d)..."

#~ msgid "Sorry"
#~ msgstr "खेद अछि"

#~ msgid "You entered two different passwords. Please try again."
#~ msgstr "अहाँ दुइ भिन्न कूटशब्द भरने छी. कृप्या फिनु सँ कोसिस करू."

#~ msgid "Unnamed"
#~ msgstr "बेनाम"

#~ msgid "&Hide"
#~ msgstr "नुकाबू (&H)"

#~ msgid "Default"
#~ msgstr "मूलभूत"

#~ msgid "Admin"
#~ msgstr "प्रशासन"

#~ msgid "Warning"
#~ msgstr "चेतावनी"

#~ msgid "Configure File Sharing..."
#~ msgstr "फाइल साझा कान्फ़िगर करू..."

#~ msgid "Select User"
#~ msgstr "प्रयोक्ताक चुनू"
